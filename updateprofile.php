<?php 
	
	include('userdata.php');
	if (isset($_POST['submit'])) {
		if (!empty($_POST['update-username']) && !empty($_POST['update-email']) && !empty($_POST['update-password'])) {
			session_start();
			$user_id = $_SESSION['user']['user_id'];
			$update_username = $_POST['update-username'];
			$update_email = $_POST['update-email'];
			$update_password = $_POST['update-password'];
			$emailverification = 0;
			$token = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890!/$()*";
		 	$token = str_shuffle($token);
		 	$token = substr($token,0,10);

		 	$sql = new Update();
		 	$result = $sql->update_profile($user_id,$update_username,$update_email,$update_password);
		 	
		 	if ($result){
				$row=mysqli_num_rows($result);
				if ($row > 0) {
					$updated=mysqli_fetch_assoc($result);
					$_SESSION['user']=$updated;
					header('location:home.php');
				}
			}
		 	else{
		 		echo "error in updating";
		 	}
		}
		else{
			echo "empty";
		}
	}
	else{
		echo "error";
	}
 ?>