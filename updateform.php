
<?php
	if (isset($_POST['submit'])){
		if (!empty($_POST['update-email']) && !empty($_POST['update-username']) && !empty($_POST['update-password'])) {
			session_start();
			$id = $_GET['id'];
			$update_email = $_POST['update-email'];
			$update_username = $_POST['update-username'];
			$update_password = $_POST['update-password'];
			include('userdata.php');
			$update_account = new Update();
			$result = $update_account->update_info($id,$update_email,$update_username,$update_password);
			
			if ($result) {
				$_SESSION['success'] = "successfully updated";
				header('location:home.php');			
			}	
			else{
				header("location:updateform.php?id=".$id);
			}
		}
	}		
			
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Update Account</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body class="register-body">
	<?php 
		include('connection.php');
		$conn= new Connection();
		$cn=$conn->connect();
		$id=$_GET['id'];
		if(empty($id)){
			header('location:showdetail.php');
		}
		$sql=mysqli_query($cn,"SELECT * FROM accounts WHERE account_id='$id' ");
		$row = '';
		if(mysqli_num_rows($sql) > 0){
			$row = mysqli_fetch_assoc($sql);
		}
		?>
	<div class="container-fluid ">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					Update Account
				</div>
				<div class="register-form">
					<form method="POST" action="" enctype="multipart/form-data">
					  <div class="form-group">
					  	<input type="hidden" name="id" value="<?php echo $_GET['account_id']; ?>">
					  	<i class="fa fa-envelope" aria-hidden="true"></i>
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email" name="update-email" class="form-control" id="u-email" value="<?php echo $row['email'];?>">
					    <i class="fa fa-user" aria-hidden="true"></i>
					    <label>Username</label>
					    <input type="text" name="update-username" class="form-control" id="u-username" value="<?php echo $row['username'];?>">
					    <i class="fa fa-key" aria-hidden="true"></i>
					    <label>Password</label>
					    <input type="password" name="update-password" class="form-control" id="u-password" value="<?php echo $row['password'];?>">
					    <br>
					    <button type="submit" name="submit" value="submit" class="btn-primary">Update</button>
					  </div>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script src="js/costum.js"></script>
</body>
</html>