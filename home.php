
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="css/materalize.css">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
	<style type="text/css">
		img{
			width: 150px;
			height: auto;
		}
		#pass{
			display: none;
		}
	</style>
</head>
<body>
	<?Php	
		include('userdata.php');
		session_start();
		$user_id = $_SESSION['user']['user_id'];
		$sql = new Selectdata();
		$result1 = $sql->select_data($user_id);
	?>
	<div class="container-fluid">
		<div class="home-nav animated fadeIn">
			<nav>
			    <div class="nav-wrapper nav-background">
			      <a href="#" class="brand-logo logo">PM
			      	<P>Password Manager</P>
			     </a>
			      <ul id="nav-mobile" class="right hide-on-med-and-down">
			      	 <li>
			        	<a class="waves-effect waves-light modal-trigger tooltipped" href="#modal2" data-position="bottom" data-delay="50" data-tooltip="Click to update profile"><?php echo $_SESSION['user']['user_name']; ?></a>
			        </li>
			        <li><a class="waves-effect waves-light  modal-trigger  tooltipped" href="#modal1" data-position="bottom" data-delay="50" data-tooltip="Click to add account">Add Account</a></li>
			        <li><a class="waves-effect waves-light modal-trigger tooltipped logout" data-position="bottom" data-delay="50" data-tooltip="Click to logout" href="logout.php">Logout</a></li>
			      </ul>
			    </div>
  			</nav>
		</div>
	</div>
	
	<div class="container-fluid">
		<div class="row first-row">
			<div class="col s6">
				<div class="addAccount">
					
					<div id="modal1" class="modal modal-fixed-footer account_model addaccount-model">
					    <div class="modal-content">
					      <h5>Add Account</h5>
					       <form method="POST" action="addaccount.php" enctype="multipart/form-data">
						       	<label>Account Type</label>
						       	<?php 
									$dropdown= new Selectdata();
									$result2=$dropdown->dropdown_data();
									echo "<select name='account-type' class='form-control'>";
									while ($row2 = mysqli_fetch_array($result2)) {
							    		echo "<option class='form-control' value='" . $row2['id'] ."'>" . $row2['account_type'] ."</option>";

									}
									echo "</select>";
								?>
								<br>
							    <label>Username</label>
						      	<input type="text" name="account-username">
						      	<label>Email</label>
						      	<input type="email" name="account-email">
						      	<label>Password</label>
						     	<input type="password" name="account-password">
						    	<button type="submit" name="submit" onclick="addAccount()" class="waves-effect waves-light btn">Submit</button>
					    	</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col s6">
				<div class="updateProfile">
					
					<div id="modal2" class="modal modal-fixed-footer account_model">
					    <div class="modal-content">
					      <h5>Update Profile</h5>
					       <form method="POST" action="updateprofile.php" enctype="multipart/form-data">
							    <label>Username</label>
						      	<input type="text" name="update-username" value="<?php echo $_SESSION['user']['user_name']?>">
						      	<label>Email</label>
						      	<input type="email" name="update-email" value="<?php echo $_SESSION['user']['user_email']?>">
						      	<label>Password</label>
						     	<input type="text" name="update-password">
						    	<button type="submit" name="submit" class="waves-effect waves-light btn">Submit</button>
					    	</form>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<?php
	$rows =mysqli_num_rows($result1);    // Find total rows returned by database
	if($rows > 0) {
		$cols = 3;    // Define number of columns
		$counter = 1;     // Counter used to identify if we need to start or end a row
		$nbsp = $cols - ($rows % $cols);    // Calculate the number of blank columns
		$container_class = 'container';  // Parent container class name
		$row_class = 'row';    // Row class name
		$col_class = 'col s4'; // Column class name
		?>

        <div class="container animated fadeIn">
        <?php
			while ($row = mysqli_fetch_array($result1)) {
				if(($counter % $cols) == 1) {
					echo '<div class="'.$row_class.'">';
				}
					echo '<div class="'.$col_class.'">';?>
						
							<div class="card hoverable">
								<div class="card-image">
								    <div class="card-logo-facebook">
								    	<?php
								    		$account_type_id = $row['account_type_id'];
								    		$sql = new Selectdata();
								    		$row_account_type = $sql->select_account_type($account_type_id);
								    	 ?>
						
								    </div>
								    <div class="card-body">
								       <a class="waves-effect modal-trigger" href="#modal<?php echo $row['account_id']; ?>">
								       	 <?php echo "<img src='logo/".$row_account_type['image']."' height = '130px' width = '130px'>"; 
								       	 ?>
								       	 <h4 class="account-type">
								       	 	<?php echo $row_account_type['account_type']; ?>		
								       	 </h4>
								       	</a>
								    </div>
								</div>
							</div>
						
						<?php echo '</div>';    // Column with content

				if(($counter % $cols) == 0) {
					echo '</div>';	 //  Close the row
				}
				$counter++;    // Increase the counter
				?>
				<div id="modal<?php echo $row['account_id']; ?>" class="modal modal-fixed-footer">
				<div class="modal-content">
					<?php echo "<img src='logo/".$row_account_type['image']."' height = '130px' width = '130px'>"; ?>
					<label>Account Id:</label><?php echo $row['account_id']; ?><br>
					<label>Account Type:</label><?php echo $row_account_type['account_type']; ?><br>
					<label>Email:</label><?php echo $row['email']; ?><br>
					<label>Username:</label><?php echo $row['username']; ?><br>
					<label>Password:</label><?php echo $row['password']; ?>
					<button type="button" class="show_password">show</button>
					<p id="pass"><?php echo $a = convert_uudecode($row['password']); ?></p>
					<br>
					<label>User Id</label><?php echo $row['user_id']; ?>
				</div>
				<div class="modal-footer">
					<div>
						<a href="updateform.php?id=<?php echo $row['account_id'];?> ">
							<button type="submit" name="submit" class="waves-effect waves-light btn" onclick="update()">Update
							</button>
						</a>
						<a href="delete.php?id=<?php echo $row['account_id']; ?>">
							<button type="submit" name="submit" class="waves-effect waves-light btn" onclick="update()">Delete
							</button>
						</a>
					</div>
				</div>
			</div>
			<?php
			}
			$result1->free();
			if($nbsp > 0) { // Adjustment to add unused column in last row if they exist
				for ($i = 0; $i < $nbsp; $i++)	{ 
					echo '<div class="'.$col_class.'">&nbsp;</div>';		
				}
				echo '</div>';  // Close the row
			}
			?>
        </div>
         <?php // Close the container
	}
	else{
		?>
		<div class="container">
			<div class="undefined" >
				<div class="card card-panel hoverable">
				    <div class="card-content">
				        <span class="card-title heading">
				             Welcome <?php echo $_SESSION['user']['user_name']; ?>
				        </span>
				        <p>
				            Curretly you have no data available. <br>
				            Please click the button "Add Account" on your left of the screen to add information. <br>
				            And to update your account information click the button "Update Profile" on your right your screen. <br><br>
				            Thank You!
				        ]</p>
				    </div>
				</div>
			</div>
		</div>
	<?php
	}
	?>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="js/costum.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
		    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
		    $('.modal').modal();
		  });
		$(document).ready(function() {
		    $('select').material_select();
		    $('.show_password').click(function(){
		    	var thisbtn = $(this);
		    	myFunction(thisbtn);
		    });

		    function myFunction(thisbtn) {
		   		// var x = document.getElementById("pass");	
		   		// x.style.display = "block";
		   		var x = thisbtn.closest(".modal-content").find('#pass');	
		   		x.show();
		   	}
		   	setTimeout(myFunction, 1000);
		  });

    </script>
</body>
</html>