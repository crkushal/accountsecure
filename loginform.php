<?php
	$login_email_value=isset($_POST['login-email']) || !empty($_POST['login-email'])?$_POST['login-email']:"";
	$login_password_value=isset($_POST['login-password']) || !empty($_POST['login-password'])?$_POST['login-password']:""; 
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Form</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
</head>
<body class="register-body">
	<div class="container-fluid ">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 register-content animated fadeIn">
				<div class="heading">
					<div class="alert alert-primary notify" role="alert">
					  <?php 
					  	session_start();
					  	if (isset($_SESSION['success'])) {
					  		echo $_SESSION['success'];
					  	}
					  	if (isset($_SESSION['error'])) {
					  		echo $_SESSION['error'];
					  	}
					  	if (isset($_SESSION['error_verify'])) {
					  		echo $_SESSION['error_verify'];
					  	}
					   ?>
					</div>
					Login
				</div>
				<div class="register-form">
					<form method="POST" action="login.php">
					  <div class="form-group">
					  	<i class="fa fa-envelope" aria-hidden="true"></i>
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email" name="login-email" class="form-control" id="l-email" aria-describedby="emailHelp" value="<?php echo $login_email_value?>">
					  </div>
					  <div class="form-group">
					  	<i class="fa fa-key" aria-hidden="true"></i>
					    <label for="exampleInputPassword1">Password</label>
					    <input type="password" name="login-password" class="form-control" id="l-password" value="<?php echo $login_email_value?>">
					  </div>
					  <button type="submit" name="submit" class="btn btn-primary" onclick="login()">Submit</button>
					</form>
				</div>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
<script src="js/costum.js"></script>
</body>
</html>