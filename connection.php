<?php 
	class Connection{
		private $server_name;
		private $server_location;
		private $server_password;
		private $db_name;

		public function connect(){
			$this->server_name = "localhost";
			$this->server_location = "root";
			$this->server_password = "";
			$this->db_name = "account_secure";

			$conn = mysqli_connect($this->server_name,$this->server_location,$this->server_password,$this->db_name);
			if ($conn) {
				return $conn;
			}
			else{
				return "error in establishing the connection";
			}
		}
	}
 ?>