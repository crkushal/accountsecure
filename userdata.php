<?php
	include_once('connection.php');
	class Insertdata{
		private $conn;
		public function __construct(){
			$connection = new Connection();
			$this->conn = $connection->connect();
		}

		public function insert_register_data($register_username,$register_email,$register_password,$emailverification,$token,$date){
			$sql="INSERT INTO users (user_name,user_email,user_password,emailverification,token,curr_date) VALUES ('$register_username','$register_email','$register_password',$emailverification,'$token' ,'$date')";
			$query = mysqli_query($this->conn,$sql);
			return $query;		
		}
		public function insert_account_data($account_type,$account_email,$account_username,$account_password,$user_id){
			$sql = "INSERT INTO accounts (account_type_id,email,username,password,user_id) VALUES ('$account_type','$account_email','$account_username','$account_password','$user_id')";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function insert_account_type($account_type,$image){
			$sql = "INSERT INTO images (account_type,image) VALUES ('$account_type',$image)";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
	} 
	class Selectdata{
		private $conn;
		public function __construct(){
			$connection = new Connection();
			$this->conn = $connection->connect();
		}
		public function select_register_data($email){
			// print_r($this->conn);
			$sql = "SELECT * FROM users WHERE user_email='$email'";
			$query = mysqli_query($this->conn, $sql);
			return $query;
		}
		public function select_login_data($login_email,$login_password){
			$sql = "SELECT * FROM users WHERE user_email='$login_email' AND user_password='$login_password'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function select_data($user_id){
			$sql = "SELECT * FROM accounts WHERE user_id='$user_id'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function dropdown_data(){
			$sql="SELECT * FROM images";
			$query=mysqli_query($this->conn,$sql);
			return $query;
		}
		public function select_account_type($account_type_id){
			$sql = "SELECT * FROM images WHERE id='$account_type_id'";
			$query = mysqli_query($this->conn,$sql);
			$result = mysqli_fetch_array($query);
			return $result;
		}
		public function email($register_email){
			$sql = "SELECT * FROM users WHERE user_email='$register_email'";
			$query = mysqli_query($this->conn,$sql);
			$result = mysqli_num_rows($query);
			return $result;
		}
	}


	class Update {
		private $conn;
		public function __construct(){
			$connection = new Connection();
			$this->conn = $connection->connect();
		}
		public function update_account($id,$update_email,$update_username,$update_password,$update_image){
			$sql = "UPDATE accounts SET email='$update_email',username='$update_username',password='$update_password',image_name='$update_image' WHERE account_id='$id'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function update_profile($user_id,$update_username,$update_email,$update_password){
			$sql = "UPDATE users SET user_name='$update_username', user_email='$update_email',user_password='$update_password' WHERE user_id='$user_id'";
			$query = mysqli_query($this->conn,$sql);
			return $query;
		}
		public function update_info($id,$update_email,$update_username,$update_password){
			$sql = "UPDATE accounts SET email='$update_email', username='$update_username',password='$update_password' WHERE account_id='$id'";
			$query= mysqli_query($this->conn,$sql);
			return $query;
		}
	}
 ?>